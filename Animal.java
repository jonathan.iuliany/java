package model;

public class Animal {

	private int quantidadePatas;
	private String ambiente;
	private double tamanho;
	private String genero;
	private String nomePopular;
	
	public Animal(String nomePopular) {
		this.nomePopular= nomePopular;
	}
	
	public int getQuantidadePatas() {
		return quantidadePatas;
	}
	public void setQuantidadePatas(int quantidadePatas) {
		this.quantidadePatas = quantidadePatas;
	}
	public String getAmbiente() {
		return ambiente;
	}
	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}
	public double getTamanho() {
		return tamanho;
	}
	public void setTamanho(double tamanho) {
		this.tamanho = tamanho;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	

	public String getNomePopular() {
		return nomePopular;
	}
	public void setNomePopular(String nomePopular) {
		this.nomePopular = nomePopular;
	}
	
	
	//Metodos
	
	public void locomover() {
		System.out.println(" o animal " + getNomePopular() +" Esta se locomovendo");
	}
	
	
	public void emitirSom() {
		System.out.println("O animal " + getNomePopular() + "Est� emitindo som");
	}
	
}
