package model;

public class Felinos extends Mamiferos{
	
	
	private String especie;
	private boolean ronrrona;
	private String habitat;
	private double massa;
	private int velocidade;
	
	


	public Felinos(String nomePopular, int numGlanduasMamarias, boolean isViviparo, boolean ronrrona) {
		super(nomePopular, numGlanduasMamarias, isViviparo);
		this.ronrrona = ronrrona;
	}



	public String getEspecie() {
		return especie;
	}



	public void setEspecie(String especie) {
		this.especie = especie;
	}



	public boolean isRonrrona() {
		return ronrrona;
	}



	public void setRonrrona(boolean ronrrona) {
		this.ronrrona = ronrrona;
	}



	public String getHabitat() {
		return habitat;
	}



	public void setHabitat(String habitat) {
		this.habitat = habitat;
	}



	public double getMassa() {
		return massa;
	}



	public void setMassa(double massa) {
		this.massa = massa;
	}



	public int getVelocidade() {
		return velocidade;
	}



	public void setVelocidade(int velocidade) {
		this.velocidade = velocidade;
	}
	
	
	//M�todos
	
	public int correr(int velocidade) {
		setVelocidade(velocidade);
		return getVelocidade();
	}
	
	public void miar() {
		
		System.out.println("O felino " + getNomePopular() + "esta miando");
	}
	
	
	public void ronrronar() {
		if(isRonrrona()) {
			System.out.println("O felino " + getNomePopular() + "est� ronrronando");
		} else {
			System.out.println("O felino " + getNomePopular() + "n�o ronrrona");
		}
	}
	
	

}
