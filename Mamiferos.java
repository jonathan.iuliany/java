package model;

public class Mamiferos extends Animal {

	private int numGlanduasMamarias;
	private String ordem;
	private boolean isViviparo;
	
	
	
	
	public Mamiferos(String nomePopular, int numGlanduasMamarias, boolean isViviparo) {
		super(nomePopular);
		this.numGlanduasMamarias = numGlanduasMamarias;
		this.isViviparo = isViviparo;
	}
	public int getNumGlanduasMamarias() {
		return numGlanduasMamarias;
	}
	public void setNumGlanduasMamarias(int numGlanduasMamarias) {
		this.numGlanduasMamarias = numGlanduasMamarias;
	}
	public String getOrdem() {
		return ordem;
	}
	public void setOrdem(String ordem) {
		this.ordem = ordem;
	}
	public boolean isViviparo() {
		return isViviparo;
	}
	public void setViviparo(boolean isViviparo) {
		this.isViviparo = isViviparo;
	}
	
	//M�todos
	
	public void amamentar() {
		System.out.println("O mam�fero " + getNomePopular() + "est� amamentando");
	}
	
	public void botarOvo() {
		if(isViviparo()) {
			System.out.println("O mam�fero " + getNomePopular() + "Bota ovos");
			
		}
	}
	
	
}
